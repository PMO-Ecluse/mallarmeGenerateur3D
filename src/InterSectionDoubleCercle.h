#pragma once

#include "Element.h"
#include "Vecteur.h"

class InterSectionDoubleCercle :
    public Element
{
public:
    InterSectionDoubleCercle(string nom, char key);
    InterSectionDoubleCercle(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~InterSectionDoubleCercle();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();

    Vecteur * vecteur()
    {
        return _vecteur;
    };

private:
    Vecteur * _vecteur;

};
