#include "Coordonnees.h"
#include "SegmentDroite.h"

Coordonnees::Coordonnees(string nom, char key, TypeTrace _type) :
    Element(nom, key)
{
    typeTrace(_type);
    initialiseOscMethodes();
}

Coordonnees::Coordonnees(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    Coordonnees(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

Coordonnees::~Coordonnees()
{
}

void Coordonnees::initialiseOscMethodes()
{
    addOscMethod("typeTrace");
}

void Coordonnees::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "typeTrace"))
    {
        if(validateOscSignature("([if])", m))
        {
            typeTrace((TypeTrace) getArgAsIntUnchecked(m, 0));
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void Coordonnees::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());

        if(typeTrace() == TRACE_REMPLIR)
        {
            if((Destination()->x < Origine()->x) and (Destination()->y < Origine()->y))
            {
                ofPushStyle();
                ofFill();
                ofSetColor(255, 255, 255, Alpha() / 2.0 * 255);
                ofRect(ofRectangle(*Destination(), *Origine()));
                ofPopStyle();
            }
        }

        ofVec2f Ox(Origine()->x, 0);
        ofVec2f Oy(0, Origine()->y);
        ofVec2f Dx(Destination()->x, ofGetHeight());
        ofVec2f Dy(ofGetWidth(), Destination()->y);
        SegmentDroite pointilles(ofxOscRouterNode::getFirstOscNodeAlias() + "-pointille", '@', TRACE_POINTILLES);
        pointilles.Dessinable(Dessinable());
        pointilles.Alpha(Alpha());
        pointilles.lineWidth(lineWidth());
        pointilles.Destination(Origine());
        pointilles.Origine(&Ox);
        pointilles.draw();
        pointilles.Origine(&Oy);
        pointilles.draw();
        pointilles.Destination(Destination());
        pointilles.Origine(&Dx);
        pointilles.draw();
        pointilles.Origine(&Dy);
        pointilles.draw();
        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

