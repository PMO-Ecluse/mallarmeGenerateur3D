#include "MultiplesTangentesParalleles.h"

MultiplesTangentesParalleles::MultiplesTangentesParalleles(string nom, char key) :
    MultiplesTangentes(nom, key)
{
}

MultiplesTangentesParalleles::MultiplesTangentesParalleles(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    MultiplesTangentesParalleles(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

MultiplesTangentesParalleles::~MultiplesTangentesParalleles()
{
}

void MultiplesTangentesParalleles::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        float distance(Tools::getDistance(*Origine(), *Destination()));
        float angle(Tools::getAngle(*Origine(), *Destination()));
        ofVec2f milieu(Origine()->getMiddle(*Destination()));
        tangente()->rayon(distance / 2.);
        tangente()->Dessinable(Dessinable());
        tangente()->Alpha(Alpha());
        tangente()->lineWidth(lineWidth());

        for(int i = 0; i < nombreDeTangentesMax(); i += _step)
        {
            tangente()->Origine(compositeur->randomizedPoints[(int)ofWrap(i, 0, 255)]);
            tangente()->angle(angle * (1 + i / (float)nombreDeTangentesMax() * M_PI));
            tangente()->draw();
        }

        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

