#include "Danseur.h"

#include "ofEvents.h"
#include "Tools.h"

/// \brief Constructeur de danseur
///
/// \param _nomDanseur const string& Le nom du danseur. Ce nom servira
/// aussi à définir le chemin OSC d'accès à l'objet.
///
///
Danseur::Danseur(const string& _nomDanseur):
    ofVec2f(200, 250),
    ofxOscRouterNode(_nomDanseur),
    toBeDrawn(DANSEUR_TIME),
    nomDanseur(_nomDanseur),
    compositeur(Tools::instance()),
    bDessinable(true),
    bAnimable(true),
    _offset(100),
    _cote(""),
    _coteAngleOffset(0),
    bHugeText(false),
    bStopCounter(false)
{
    _cotePosition.set(0, 0);
    _alpha.reset(0);
    _alpha.setDuration(1);
    _alpha.setRepeatType(PLAY_ONCE);
    _alpha.setCurve(EASE_IN_EASE_OUT);
    _coteAngle.reset(0);
    _coteAngle.setDuration(60);
    _coteAngle.setRepeatType(LOOP);
    _coteAngle.setCurve(LINEAR);
    _coteAngle.animateTo(2 * M_PI);
    ofAddListener(ofEvents().setup, this, &Danseur::setup);
    ofAddListener(ofEvents().update, this, &Danseur::update);
    initialiseOscMethodes();
}

/// \brief Destructeur
///
Danseur::~Danseur()
{
    delete _ttfText;
    delete _ttfTextHuge;
    _ttfText = _ttfTextHuge = NULL;
}

void Danseur::initialiseOscMethodes()
{
    addOscMethod("position");
    addOscMethod("dessinable");
    addOscMethod("animable");
    addOscMethod("apparait");
    addOscMethod("disparait");
    addOscMethod("alpha");
    addOscMethod("duration");
    addOscMethod("offset");
    addOscMethod("grand");
    addOscMethod("afficherTemps");
    addOscMethod("afficherXPos");
    addOscMethod("afficherYPos");
    addOscMethod("afficherAngle");
    addOscMethod("afficherConstante");
    addOscMethod("afficherAll");
    addOscMethod("stopCounter");
    addOscMethod("vitesseRotation");
}

/// \brief Surcharge de l'opérateur de flux de sortie
///
/// \param output ostream& Le flux de sortie
/// \param composition const Danseur& Le danseur courant
/// \return ostream& Le flux de sortie
///
///
ostream& operator<<(ostream& output, const Danseur& _d)
{
    output << _d.x << ", " << _d.y;
    return output;
}

/// \brief Callback oF de définitions utilisé ici pour finir de construire l'objet
///
/// \param args ofEventArgs&
/// \return void
///
///
void Danseur::setup()
{
    _ttfText = new ofTrueTypeFont();
    _ttfTextHuge = new ofTrueTypeFont();
    _ttfText->loadFont("/usr/share/fonts/truetype/dejavu/DejaVuSans-Oblique.ttf", 13, true, true);
    _ttfText->setEncoding(OF_ENCODING_UTF8);
    _ttfTextHuge->loadFont("/usr/share/fonts/truetype/dejavu/DejaVuSans-Oblique.ttf", 44, true, true);
    _ttfTextHuge->setEncoding(OF_ENCODING_UTF8);
    _coteAngleOffset = (uint16_t)ofRandom(0, 360);
    ofLogVerbose("mallarme3D") << nomDanseur << " is created";
}

/// \brief Callback de mise à jour des contenus (le cas échéant)
///
/// \return void
///
///
void Danseur::update()
{
    float dt = 0.016666666;
    _alpha.update(dt);

    if(Animable())
    {
        _coteAngle.update(dt);
    }

    if(!bStopCounter)
    {
        _cote = ofGetTimestampString("%H%M%S%F");
    }
}

/// \brief Callback de dessin
///
/// \return void
///
///
void Danseur::draw()
{
    if(Dessinable())
    {
        if(toBeDrawn)
        {
            ofPushStyle();
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, Alpha() * 255);
            ofTrueTypeFont * text = _ttfText;

            if(bHugeText)
            {
                text = _ttfTextHuge;
            }

            float angleStep(0);
            float angle(0);

            if(Animable())
            {
                angleStep = 360 / 5.0f;
                angle = ofRadToDeg(_coteAngle.getCurrentValue()) + _coteAngleOffset;
            }

            ofPushStyle();
            ofPushMatrix();
            ofTranslate(x, y);

            if(toBeDrawn & DANSEUR_TIME)
            {
                angle += angleStep;
                ofPushMatrix();
                ofRotateZ(angle);
                text->drawString(_cote, offset(), 0);
                ofPopMatrix();
            }

            if(Animable())
            {
                if(toBeDrawn & DANSEUR_XPOS)
                {
                    angle += angleStep;
                    ofPushMatrix();
                    ofRotateZ(angle);
                    text->drawString("x=" + ofToString(x), offset(), 0);
                    ofPopMatrix();
                }

                if(toBeDrawn & DANSEUR_YPOS)
                {
                    angle += angleStep;
                    ofPushMatrix();
                    ofRotateZ(angle);
                    text->drawString("y=" + ofToString(y), offset(), 0);
                    ofPopMatrix();
                }

                if(toBeDrawn & DANSEUR_ANGLE)
                {
                    angle += angleStep;
                    ofPushMatrix();
                    ofRotateZ(angle);
                    ofVec2f O(ofGetWidth() / 2.0f, ofGetHeight() / 2.0f);
                    std::stringstream valeurAngle;
                    valeurAngle << std::fixed << std::setprecision(2) << Tools::getAngle((*this), O);
                    text->drawString("T=" + valeurAngle.str() + "°", offset(), 0);
                    ofPopMatrix();
                }

                if(toBeDrawn & DANSEUR_CONSTANTE)
                {
                    angle += angleStep;
                    ofPushMatrix();
                    ofRotateZ(angle);
                    text->drawString("a=" + ofToString(M_PI), offset(), 0);
                    ofPopMatrix();
                }
            }

            ofPopMatrix();
            ofDisableAlphaBlending();
            ofPopStyle();
        }
    }
}

void Danseur::apparait(float valDestination)
{
    _alpha.animateTo(valDestination);
}

void Danseur::disparait()
{
    _alpha.animateTo(0);
}

/// \brief Processeur de message OSC
///
/// Le processeur s'attend à ce que la command soit :
///     'position [i][i]?', 'position [f][f]?' ou 'taille [if]'
///
/// \param command const string& La commande OSC
/// \param m const ofxOscMessage& La(es) valeur(s) associée(s) à la commande
/// \return void
///
///
void Danseur::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "position"))
    {
        if(validateOscSignature("([i][i]?)", m)) /**< Position absolue en pixel sur l'écran */
        {
            if(m.getNumArgs() == 1)
            {
                ofVec2f::set(getArgAsIntUnchecked(m, 0)); /**< Défini la position avec X=Y */
            }
            else
            {
                ofVec2f::set(getArgAsIntUnchecked(m, 0), getArgAsIntUnchecked(m, 1)); /**< Défini la position par les attributs OSC */
            }
        }
        else if(validateOscSignature("([if][if]?)", m)) /**< Position proportionnelle à la taille de l'écran */
        {
            if(m.getNumArgs() == 1)
            {
                ofVec2f::set(getArgAsFloatUnchecked(m, 0)); /**< Défini la position avec X=Y */
            }
            else
            {
                ofVec2f::set(getArgAsFloatUnchecked(m, 0) * ofGetWidth(), getArgAsFloatUnchecked(m, 1) * ofGetHeight()); /**< Défini la position par les attributs OSC */
            }
        }
    }
    else if(isMatch(command, "dessinable"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            Dessinable(getArgAsBoolUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "animable"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            Animable(getArgAsBoolUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "apparait"))
    {
        if(validateOscSignature("([TFif][if]?[if]?)", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                if(m.getNumArgs() >= 2)
                {
                    if(m.getNumArgs() == 3)
                    {
                        _alpha.setDuration(getArgAsFloatUnchecked(m, 2));
                    }

                    apparait(getArgAsFloatUnchecked(m, 1));
                }
                else
                {
                    apparait();
                }
            }
            else
            {
                if(m.getNumArgs() == 2)
                {
                    _alpha.setDuration(getArgAsFloatUnchecked(m, 1));
                }

                disparait();
            }
        }
    }
    else if(isMatch(command, "disparait"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                disparait();
            }
        }
    }
    else if(isMatch(command, "alpha"))
    {
        if(validateOscSignature("([if])", m))
        {
            Alpha(getArgAsFloatUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "duration"))
    {
        if(validateOscSignature("([if])", m))
        {
            _alpha.setDuration(getArgAsFloatUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "offset"))
    {
        if(validateOscSignature("([if])", m))
        {
            offset(getArgAsIntUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "grand"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            bHugeText = getArgAsBoolUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "afficherTemps"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                toBeDrawn |= DANSEUR_TIME;
            }
            else
            {
                toBeDrawn &= ~DANSEUR_TIME;
            }
        }
    }
    else if(isMatch(command, "afficherXPos"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                toBeDrawn |= DANSEUR_XPOS;
            }
            else
            {
                toBeDrawn &= ~DANSEUR_XPOS;
            }
        }
    }
    else if(isMatch(command, "afficherYPos"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                toBeDrawn |= DANSEUR_YPOS;
            }
            else
            {
                toBeDrawn &= ~DANSEUR_YPOS;
            }
        }
    }
    else if(isMatch(command, "afficherAngle"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                toBeDrawn |= DANSEUR_ANGLE;
            }
            else
            {
                toBeDrawn &= ~DANSEUR_ANGLE;
            }
        }
    }
    else if(isMatch(command, "afficherConstante"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                toBeDrawn |= DANSEUR_CONSTANTE;
            }
            else
            {
                toBeDrawn &= ~DANSEUR_CONSTANTE;
            }
        }
    }
    else if(isMatch(command, "afficherAll"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                toBeDrawn = DANSEUR_ALL;
            }
            else
            {
                toBeDrawn = DANSEUR_TIME;
            }
        }
    }
    else if(isMatch(command, "stopCounter"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            bStopCounter = getArgAsBoolUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "vitesseRotation"))
    {
        if(validateOscSignature("([if])", m))
        {
            _coteAngle.setDuration(getArgAsFloatUnchecked(m, 0));
        }
    }
}

