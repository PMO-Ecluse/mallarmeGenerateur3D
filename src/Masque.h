#ifndef MASQUE_H
#define MASQUE_H

#include <ofxOscRouterNode.h>

class Masque : public ofxOscRouterNode
{
public:

    static Masque * instance();
    void update();
    void draw();
    /** Default destructor */
    virtual ~Masque();
    /** Access m_TLCorner
     * \return The current value of m_TLCorner
     */
    ofVec3f GetTLCorner()
    {
        return m_TLCorner;
    }
    /** Set m_TLCorner
     * \param val New value to set
     */
    void SetTLCorner(ofVec3f val)
    {
        m_TLCorner = val;
    }
    /** Access m_BLCorner
     * \return The current value of m_BLCorner
     */
    ofVec3f GetBLCorner()
    {
        return m_BLCorner;
    }
    /** Set m_BLCorner
     * \param val New value to set
     */
    void SetBLCorner(ofVec3f val)
    {
        m_BLCorner = val;
    }
    /** Access m_BRCorner
     * \return The current value of m_BRCorner
     */
    ofVec3f GetBRCorner()
    {
        return m_BRCorner;
    }
    /** Set m_BRCorner
     * \param val New value to set
     */
    void SetBRCorner(ofVec3f val)
    {
        m_BRCorner = val;
    }
    /** Access m_TRCorner
     * \return The current value of m_TRCorner
     */
    ofVec3f GetTRCorner()
    {
        return m_TRCorner;
    }
    /** Set m_TRCorner
     * \param val New value to set
     */
    void SetTRCorner(ofVec3f val)
    {
        m_TRCorner = val;
    }
    void processOscCommand(const string& command, const ofxOscMessage& m);
protected:
private:
    /** Default constructor */
    Masque();
    static Masque * singleton;
    ofMesh masqueMesh;
    ofVec3f m_TLCorner; //!< Member variable "m_TLCorner"
    ofVec3f m_BLCorner; //!< Member variable "m_BLCorner"
    ofVec3f m_BRCorner; //!< Member variable "m_BRCorner"
    ofVec3f m_TRCorner; //!< Member variable "m_TRCorner"
    ofFloatColor masqueColor;
};

#endif // MASQUE_H
