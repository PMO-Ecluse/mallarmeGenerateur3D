#include "Core.h"
#include "Tools.h"
#include "Masque.h"

//--------------------------------------------------------------
/// \brief Callback de construction de l'application
///
/// \return void
///
///
void Core::setup()
{
    ofSetFrameRate(60);
#ifdef TARGET_OPENGLES
    ofHideCursor();
#endif // TARGET_OPENGLES
    /* Récupération et configuration des singleton */
    compositeur = Tools::instance();
    compositeur->initializeRandomizedPoints();
    ofSetStyle(*compositeur->styleStandard);
    ofBackground(ofColor::black);
    masqueCadrage = Masque::instance();
    sceneManager = ofxSceneManager::instance();
#ifdef DEBUG
    sceneManager->setDrawDebug(true);
#endif // DEBUG
    sceneManager->setCurtainDropTime(1.0);
    sceneManager->setCurtainStayTime(0.0); // 0 correspond à +inf.
    sceneManager->setCurtainRiseTime(1.0);
    sceneManager->setOverlapUpdate(true);
    /* Configuration OSC */
    ofxOscRouter::addOscChild(compositeur);
    ofxOscRouter::addOscChild(masqueCadrage);
    ofxOscRouter::addOscChild(&Florence);
    ofxOscRouter::addOscChild(&Annabelle);
    oscSend.setup("localhost", 9876);
    // ++++++++
    // Initialisation des scenes : pointeurs + sceneManager + OscChild
    //
    // vvvvvvvv
    //          sceneElements ========
    ptr_sceneElements = new sceneElements();
    sceneManager->addScene(ptr_sceneElements, Core::START);
    ofxOscRouter::addOscChild(ptr_sceneElements);
    //          scenePassageATraversCarresBlanc ========
    ptr_scenePassageATraversCarresBlanc = new scenePassageATraversCarresBlanc();
    sceneManager->addScene(ptr_scenePassageATraversCarresBlanc, Core::PASSAGE_A_TRAVERS_CARRES);
    ofxOscRouter::addOscChild(ptr_scenePassageATraversCarresBlanc);
#ifdef DEBUG
    sceneManager->listScenes();
#endif // DEBUG
}

//--------------------------------------------------------------
/// \brief Callback de mise à jour des contenus (le cas échéant)
///
/// \return void
///
///
void Core::update()
{
    ofxOscRouter::update();
    float dt = 0.016666666;
    sceneManager->update(dt);
    sceneManager->setDrawDebug(bDebug);
    masqueCadrage->update();
}

//--------------------------------------------------------------
/// \brief Callback de dessin
///
/// \return void
///
///
void Core::draw()
{
    if(!bBlackOut)
    {
        ofSetStyle(*compositeur->styleStandard);
        sceneManager->draw();
        Annabelle.draw();
        Florence.draw();
        masqueCadrage->draw();
    }
}

/// \brief Callback de sortie de programme (nettoyage au besoin)
///
/// \return void
///
///
void Core::exit()
{
    ofGetWindowPtr()->windowShouldClose();
}

/// \brief Processeur de message OSC
///
///
///
/// \param command const string& La commande OSC
/// \param m const ofxOscMessage& La(es) valeur(s) associée(s) à la commande
/// \return void
///
///
void Core::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "exit"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                exit();
            }
        }
    }
    else if(isMatch(command, "debug"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            bDebug = getArgAsBoolUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "switchToScene"))
    {
        if(validateOscSignature("([i])", m))
        {
            sceneManager->goToScene(getArgAsIntUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "blackout"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            bBlackOut = getArgAsBoolUnchecked(m, 0);
            Annabelle.Dessinable(!bBlackOut);
            Florence.Dessinable(!bBlackOut);
        }
    }
}

//--------------------------------------------------------------
/// \brief Callback de gestion du clavier pressé
///
/// \param key int La touche clavier pressée
/// \return void
///
///
void Core::keyPressed(int key)
{
    if(key == OF_KEY_RETURN)
    {
        sceneManager->goToScene(sceneManager->getCurrentSceneID() + 1);
    }
    else if(key >= '0' and key <= '9')
    {
        int keyVal(key - 48);

        if(sceneManager->getCurrentSceneID() != keyVal)
        {
            sceneManager->goToScene(keyVal, true, false);
        }
    }
    else if(key == 'D')
    {
        bDebug = !bDebug;
    }
    else if(key == ' ')
    {
        m.clear();
        m.setAddress("/mallarme3D/annabelle/position");
        m.addIntArg(ofGetMouseX());
        m.addIntArg(ofGetMouseY());
        oscSend.sendMessage(m);
    }

    sceneManager->keyPressed(key);
}

//--------------------------------------------------------------
/// \brief Callback de gestion du clavier relaché
///
/// \param key int La touche clavier relachée
/// \return void
///
///
void Core::keyReleased(int key)
{
}

//--------------------------------------------------------------
/// \brief Callback de gestion du mouvement de la souris
///
/// \param x int Coordonnée X du pointeur
/// \param y int Coordonnée Y du pointeur
/// \return void
///
///
void Core::mouseMoved(int x, int y)
{
    m.clear();
    m.setAddress("/mallarme3D/florence/position");
    m.addIntArg(ofGetMouseX());
    m.addIntArg(ofGetMouseY());
    oscSend.sendMessage(m);
}

//--------------------------------------------------------------
/// \brief Callback de gestion de la pression des boutons de la souris
///
/// \param x int Coordonnée X du pointeur au moment de la pression
/// \param y int Coordonnée Y du pointeur au moment de la pression
/// \param button int Lequel des boutons est pressé
/// \return void
///
///
void Core::mousePressed(int x, int y, int button)
{
    m.clear();
    m.setAddress("/mallarme3D/blackout");
    m.addBoolArg(true);
    oscSend.sendMessage(m);
}

//--------------------------------------------------------------
/// \brief Callback de gestion de la relâche des boutons de la souris
///
/// \param x int Coordonnée X du pointeur au moment de la relâche
/// \param y int Coordonnée Y du pointeur au moment de la relâche
/// \param button int Lequel des boutons est relaché
/// \return void
///
///
void Core::mouseReleased(int x, int y, int button)
{
    m.clear();
    m.setAddress("/mallarme3D/blackout");
    m.addBoolArg(false);
    oscSend.sendMessage(m);
}

//--------------------------------------------------------------
/// \brief Callback de gestion du redimensionnement de la fenetre
///
/// \param w int Nouvelle largeur de la fenetre
/// \param h int Nouvelle hauteur de la fenetre
/// \return void
///
///
void Core::windowResized(int w, int h)
{
    sceneManager->windowResized(w, h);
}

