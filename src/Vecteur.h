#pragma once

#include "Element.h"
#include "SegmentDroite.h"

class Vecteur :
    public Element
{
public:
    Vecteur(string nom, char key, float stealthSize = 3.0f, TypeTrace _type = TRACE_LIGNE);
    Vecteur(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination, float stealthSize = 3.0f, TypeTrace _type = TRACE_LIGNE);
    virtual ~Vecteur();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();

    SegmentDroite* segment()
    {
        return &_segment;
    };

    void stealthSize(float _size)
    {
        _stealthSize = _size;
    };
    float stealthSize()
    {
        return _stealthSize;
    };

private:
    SegmentDroite _segment;
    float _stealthSize;
};
