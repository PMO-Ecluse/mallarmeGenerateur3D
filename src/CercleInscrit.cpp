#include "CercleInscrit.h"

CercleInscrit::CercleInscrit(string nom, char key) :
    Element(nom, key)
{
}

CercleInscrit::CercleInscrit(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    CercleInscrit(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

CercleInscrit::~CercleInscrit()
{
}

void CercleInscrit::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        float distance(Tools::getDistance(*Origine(), *Destination()));
        float rayonPetit(50);
        float rayonGrand(rayonPetit + distance);
        ofCircle(*Origine(), rayonPetit);
        ofCircle(*Destination(), rayonGrand);
        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

