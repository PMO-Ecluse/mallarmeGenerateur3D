#include "Triangle.h"
#include "SegmentDroite.h"

Triangle::Triangle(string nom, char key, ofVec2f * _pointIntermediaire, TypeTrace _type) :
    Element(nom, key),
    pointIntermediaire(_pointIntermediaire)
{
    typeTrace(_type);
}

Triangle::Triangle(string nom, char key,
                   ofVec2f * _pointOrigine,
                   ofVec2f * _pointIntermediaire,
                   ofVec2f * _pointDestination, TypeTrace _type) :
    Triangle(nom, key, _pointIntermediaire, _type)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

Triangle::~Triangle()
{
}

void Triangle::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());

        if(typeTrace() == TRACE_POINTILLES)
        {
            std::array<ofVec2f *, 3> points {Origine(), pointIntermediaire, Destination()};

            for(unsigned int i = 0; i < 3; i++)
            {
                SegmentDroite segment(ofToString(i), '@', points[i], points[(i + 1) % 3], TRACE_POINTILLES);
                segment.Dessinable(Dessinable());
                segment.Alpha(Alpha());
                segment.lineWidth(lineWidth());
                segment.offset(offset());
                segment.draw();
            }
        }
        else
        {
            if(typeTrace() == TRACE_LIGNE)
            {
                ofNoFill();
            }
            else
            {
                ofFill();
            }

            ofTriangle(*Origine(), *pointIntermediaire, *Destination());
        }

        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

