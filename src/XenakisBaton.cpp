#include "XenakisBaton.h"

XenakisBaton::XenakisBaton(string nom, char key, ofVec2f * _pointOrigine) :
    Xenakis(nom, key, _pointOrigine)
{
}

XenakisBaton::~XenakisBaton()
{
}

void XenakisBaton::draw()
{
    if(Dessinable())
    {
        if(!xenakisQueue.empty())
        {
            ofPushStyle();
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, Alpha() * 255);
            ofSetLineWidth(lineWidth());
            ofVec2f previousPoint(xenakisQueue.front());

            for(itXenakisQueue = xenakisQueue.begin(); itXenakisQueue != xenakisQueue.end(); itXenakisQueue++)
            {
                ofPushMatrix();
                ofTranslate(*itXenakisQueue);
                ofRotateZ(Tools::getAngle(previousPoint, *itXenakisQueue));
                ofLine(0, -offset(), 0, offset());
                ofPushStyle();
                ofFill();
                ofCircle(0, -offset(), 1.3 * lineWidth());
                ofCircle(0, offset(), 1.3 * lineWidth());
                ofPopStyle();
                ofPopMatrix();
                previousPoint.set(*itXenakisQueue);
            }

            ofDisableAlphaBlending();
            ofPopStyle();
        }
    }
}

