#pragma once

#include "Element.h"

class CercleInscrit :
    public Element
{
public:
    CercleInscrit(string nom, char key);
    CercleInscrit(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~CercleInscrit();
    void draw();
};
