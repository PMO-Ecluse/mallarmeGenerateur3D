#pragma once

#include "Element.h"
#include "Xenakis.h"

class XenakisBaton :
    public Xenakis
{
public:
    XenakisBaton(string nom, char key, ofVec2f * _pointOrigine);
    virtual ~XenakisBaton();
    void draw();
};
