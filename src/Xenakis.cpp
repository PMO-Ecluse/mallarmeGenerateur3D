#include "Xenakis.h"

Xenakis::Xenakis(string nom, char key, ofVec2f * _pointOrigine) :
    Element(nom, key),
    _nombre(20),
    _delay(0.5f)
{
    offset(10);
    initialiseOscMethodes();
    Origine(_pointOrigine);
}

Xenakis::~Xenakis()
{
}

void Xenakis::update(float dt)
{
    if(Dessinable())
    {
        Element::update(dt);

        if(!xenakisQueue.empty() and xenakisQueue.size() >= (unsigned int)nombre())
        {
            xenakisQueue.pop_front();
        }

        int delay(ofGetFrameRate() * _delay);

        if(delay < 1)
        {
            delay = 1;
        }

        if(ofGetFrameNum() % delay == 0)
        {
            xenakisQueue.push_back(*Origine());
        }
    }
}

void Xenakis::initialiseOscMethodes()
{
    addOscMethod("nombre");
    addOscMethod("delay");
}

void Xenakis::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "nombre"))
    {
        if(validateOscSignature("([if])", m))
        {
            nombre(getArgAsIntUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "delay"))
    {
        if(validateOscSignature("([if])", m))
        {
            delay(getArgAsFloatUnchecked(m, 0));
        }
    }

    Element::processOscCommand(command, m);
}

