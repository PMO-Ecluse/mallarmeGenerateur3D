#pragma once

#include "ofMain.h"
#include "ofxOscRouterNode.h"

class Tools : public ofxOscRouterNode
{
public:
    ~Tools() {};
    static Tools * instance();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void initializeRandomizedPoints();
    std::vector<ofVec2f *> randomizedPoints; ///< Vecteur de points aléatoirement disposés sur le plan
    static float getDistance(const ofVec2f & _origine, const ofVec2f & _destination);
    static float getAngle(const ofVec2f & _origine, const ofVec2f & _destination);
    static ofVec2f getIntersectionPoint(const ofVec2f& A, const ofVec2f& B, const ofVec2f& C, const ofVec2f& D);
    static ofStyle * styleStandard;

private:
    Tools();
    static Tools * singleton;
};
