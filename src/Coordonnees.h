#pragma once

#include "Element.h"

class Coordonnees :
    public Element
{
public:
    Coordonnees(string nom, char key, TypeTrace _type = TRACE_LIGNE);
    Coordonnees(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~Coordonnees();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();
};
