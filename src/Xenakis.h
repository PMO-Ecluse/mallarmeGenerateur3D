#pragma once

#include "Element.h"

class Xenakis :
    public Element
{
public:
    Xenakis(string nom, char key, ofVec2f * _pointOrigine);
    virtual ~Xenakis();
    void update(float dt);
    void processOscCommand(const string& command, const ofxOscMessage& m);
    virtual void draw() = 0;

    void nombre(int _val)
    {
        _nombre = _val;
    };
    int nombre()
    {
        return _nombre;
    };

    void delay(float _val)
    {
        _delay = _val;
    };
    float delay()
    {
        return _delay;
    };

protected:
    std::deque<ofVec2f> xenakisQueue;
    std::deque<ofVec2f>::iterator itXenakisQueue;

private:
    void initialiseOscMethodes();
    int _nombre;
    float _delay;
};
