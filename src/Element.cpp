#include "Element.h"

Element::Element(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination, TypeTrace _type) :
    ofxOscRouterNode(nom),
    compositeur(Tools::instance()),
    origine(_pointOrigine),
    destination(_pointDestination),
    bDessinable(false),
    bAnimable(true),
    _lineWidth(ofGetStyle().lineWidth),
    _offset(20),
    _activeKey(key),
    _typeTrace(_type)
{
    _alpha.reset(0);
    _alpha.setDuration(1);
    _alpha.setRepeatType(PLAY_ONCE);
    _alpha.setCurve(EASE_IN_EASE_OUT);
    initialiseOscMethodes();
}

Element::~Element()
{
}

void Element::initialiseOscMethodes()
{
    addOscMethod("dessinable");
    addOscMethod("animable");
    addOscMethod("apparait");
    addOscMethod("disparait");
    addOscMethod("alpha");
    addOscMethod("duration");
    addOscMethod("lineWidth");
    addOscMethod("offset");
    addOscMethod("typeTrace");
}

void Element::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "dessinable"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            Dessinable(getArgAsBoolUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "animable"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            Animable(getArgAsBoolUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "apparait"))
    {
        if(validateOscSignature("([TFif][if]?[if]?)", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                if(m.getNumArgs() >= 2)
                {
                    if(m.getNumArgs() == 3)
                    {
                        _alpha.setDuration(getArgAsFloatUnchecked(m, 2));
                    }

                    apparait(getArgAsFloatUnchecked(m, 1));
                }
                else
                {
                    apparait();
                }
            }
            else
            {
                if(m.getNumArgs() == 2)
                {
                    _alpha.setDuration(getArgAsFloatUnchecked(m, 1));
                }

                disparait();
            }
        }
    }
    else if(isMatch(command, "disparait"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                disparait();
            }
        }
    }
    else if(isMatch(command, "alpha"))
    {
        if(validateOscSignature("([if])", m))
        {
            Alpha(getArgAsFloatUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "duration"))
    {
        if(validateOscSignature("([if])", m))
        {
            _alpha.setDuration(getArgAsFloatUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "lineWidth"))
    {
        if(validateOscSignature("([if])", m))
        {
            lineWidth(getArgAsFloatUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "offset"))
    {
        if(validateOscSignature("([if])", m))
        {
            offset(getArgAsIntUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "typeTrace"))
    {
        if(validateOscSignature("([if])", m))
        {
            typeTrace((TypeTrace) getArgAsIntUnchecked(m, 0));
        }
    }
}

void Element::update(float dt)
{
    if(Animable())
    {
        _alpha.update(dt);
    }
}

void Element::draw()
{
    if(Dessinable())
    {
        ofDrawBitmapString(ofxOscRouterNode::getFirstOscNodeAlias(), ofGetWidth() / 2, ofGetHeight() / 2);
    }
}

void Element::apparait(float valDestination)
{
    _alpha.animateTo(valDestination);
}

void Element::disparait()
{
    _alpha.animateTo(0);
}

void Element::Origine(ofVec2f * _origine)
{
    origine = _origine;
}

ofVec2f * Element::Origine()
{
    return origine;
}

void Element::Destination(ofVec2f * _destination)
{
    destination = _destination;
}

ofVec2f * Element::Destination()
{
    return destination;
}

