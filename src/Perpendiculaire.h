#pragma once

#include "Element.h"

class Perpendiculaire :
    public Element
{
public:
    Perpendiculaire(string nom, char key, float _position = 0.2);
    Perpendiculaire(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination, float _position = 0.2);
    virtual ~Perpendiculaire();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();
private:
    bool showSymbole;
protected:
    float positionRelative;
};
