#include "MultiplesTangentes.h"

MultiplesTangentes::MultiplesTangentes(string nom, char key) :
    Element(nom, key),
    _tangente(new Tangente(ofxOscRouterNode::getFirstOscNodeAlias() + "-tangente", '@')),
    _nombreDeTangentesMax(300)
{
    nombreDeTangentes(20);
    initialiseOscMethodes();
}

MultiplesTangentes::MultiplesTangentes(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    MultiplesTangentes(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

MultiplesTangentes::~MultiplesTangentes()
{
}

void MultiplesTangentes::initialiseOscMethodes()
{
    addOscMethod("nombre");
}

void MultiplesTangentes::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "nombre"))
    {
        if(validateOscSignature("([if])", m))
        {
            nombreDeTangentes(getArgAsIntUnchecked(m, 0));
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void MultiplesTangentes::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        float distance(Tools::getDistance(*Origine(), *Destination()));
        float angle(Tools::getAngle(*Origine(), *Destination()));
        ofVec2f milieu(Origine()->getMiddle(*Destination()));
        tangente()->Origine(&milieu);
        tangente()->rayon(distance / 2.);
        tangente()->Dessinable(Dessinable());
        tangente()->Alpha(Alpha());
        tangente()->lineWidth(lineWidth());

        for(int i = 0; i < nombreDeTangentesMax(); i += _step)
        {
            tangente()->angle(angle * (1 + i / (float)nombreDeTangentesMax() * M_PI));
            tangente()->draw();
        }

        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

