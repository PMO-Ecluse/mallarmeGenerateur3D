#include "Vecteur.h"

Vecteur::Vecteur(string nom, char key, float stealthSize, TypeTrace _type) :
    Element(nom, key),
    _segment(ofxOscRouterNode::getFirstOscNodeAlias() + "-segment", '@'),
    _stealthSize(stealthSize)
{
    typeTrace(_type);
    initialiseOscMethodes();
}

Vecteur::Vecteur(string nom, char key,
                 ofVec2f * _pointOrigine, ofVec2f * _pointDestination,
                 float stealthSize, TypeTrace _type):
    Vecteur(nom, key, stealthSize, _type)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

Vecteur::~Vecteur()
{
}

void Vecteur::initialiseOscMethodes()
{
    addOscMethod("stealthSize");
}

void Vecteur::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "stealthSize"))
    {
        if(validateOscSignature("([if])", m))
        {
            stealthSize(getArgAsFloatUnchecked(m, 0));
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void Vecteur::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        float distance(Tools::getDistance(*Origine(), *Destination()));
        float angle(Tools::getAngle(*Origine(), *Destination()));
        float stealthRealSize(lineWidth() * stealthSize());
        ofVec2f origine(0, 0);
        ofVec2f destination(distance, 0);
        ofFill();
        ofPushMatrix();
        ofTranslate(*Origine());
        ofRotateZ(angle);
        ofTriangle(distance, 0, distance - stealthRealSize,
                   stealthRealSize / 2, distance - stealthRealSize,
                   -stealthRealSize / 2);
        destination.x -= stealthRealSize - 1;
        ofTriangle(0, 0,
                   stealthRealSize, stealthRealSize / 2,
                   stealthRealSize, -stealthRealSize / 2);
        origine.x += stealthRealSize - 1;
        _segment.Origine(&origine);
        _segment.Destination(&destination);

        if(typeTrace() == TRACE_POINTILLES)
        {
            _segment.typeTrace(TRACE_POINTILLES);
        }
        else
        {
            _segment.typeTrace(typeTrace());
        }

        _segment.Dessinable(Dessinable());
        _segment.Alpha(Alpha());
        _segment.lineWidth(lineWidth());
        _segment.draw();
        ofPopMatrix();
        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

