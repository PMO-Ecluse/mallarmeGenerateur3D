#pragma once

#include "Element.h"

class EllipseDouble :
    public Element
{
public:
    EllipseDouble(string nom, char key);
    EllipseDouble(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~EllipseDouble();
    void draw();
};
