#include "CercleDouble.h"

CercleDouble::CercleDouble(string nom, char key) :
    Element(nom, key)
{
}

CercleDouble::CercleDouble(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    CercleDouble(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

CercleDouble::~CercleDouble()
{
}

void CercleDouble::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        float rayonCercle(200);
        ofSetLineWidth(lineWidth());
        ofCircle(*Origine(), rayonCercle);
        ofCircle(*Destination(), rayonCercle);
        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

