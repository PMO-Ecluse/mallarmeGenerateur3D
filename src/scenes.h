#pragma once

#include <ofxScene.h>
#include <ofxBox2d.h>

#include "Tools.h"
#include "Danseur.h"
#include "Element.h"

#include "Angle.h"
#include "CercleDouble.h"
#include "CerclesImmenses.h"
#include "CercleInscrit.h"
#include "Coordonnees.h"
#include "CourbeBezier.h"
#include "Distance.h"
#include "EllipseDouble.h"
#include "InterSectionDoubleCercle.h"
#include "Mediatrice.h"
#include "MultiplesTangentes.h"
#include "MultiplesTangentesParalleles.h"
#include "Perpendiculaire.h"
#include "SegmentDroite.h"
#include "Tangente.h"
#include "Triangle.h"
#include "Vecteur.h"
#include "XenakisCercle.h"
#include "XenakisDroite.h"
#include "XenakisBaton.h"
#include "XenakisData.h"


class Scene :
    public ofxScene,
    public ofxOscRouterNode
{
public:
    Tools * compositeur;
    std::vector<Element*> collectionDesElements;
    std::vector<Element*>::iterator dictIterator;

    Scene(std::string nomScene) :
        ofxScene(),
        ofxOscRouterNode(nomScene),
        compositeur(Tools::instance())
    {};
    virtual ~Scene() {};

    virtual void setup() {};
    virtual void processOscCommand(const string& command, const ofxOscMessage& m) {};
    virtual void update(float dt)
    {
        for(dictIterator = collectionDesElements.begin();
                dictIterator != collectionDesElements.end();
                ++dictIterator)
        {
            (*dictIterator)->update(dt);
        }
    };
    virtual void draw()
    {
        for(dictIterator = collectionDesElements.begin();
                dictIterator != collectionDesElements.end();
                ++dictIterator)
        {
            (*dictIterator)->draw();
        }
    };
    virtual void keyPressed(int key)
    {
        for(dictIterator = collectionDesElements.begin();
                dictIterator != collectionDesElements.end();
                ++dictIterator)
        {
            if(key == 'a')
            {
                (*dictIterator)->Toggle();
            }
            else if(key == (*dictIterator)->Key())
            {
                (*dictIterator)->Toggle();
            }
        }
    };
};

/// \brief Scene contenant tout les éléments dessinables
///
class sceneElements :
    public Scene
{
public:
    sceneElements() :
        Scene("elements")
    {
        collectionDesElements.push_back(new SegmentDroite("pointilles", 'z', Element::TRACE_POINTILLES));
        collectionDesElements.push_back(new CourbeBezier("bezier", 'e', compositeur->randomizedPoints[123], compositeur->randomizedPoints[124]));
        collectionDesElements.push_back(new Perpendiculaire("perpendiculaire", 'r'));
        collectionDesElements.push_back(new Perpendiculaire("mediatrice", 't', 0.6));
        collectionDesElements.push_back(new Tangente("tangente", 'y'));
        collectionDesElements.push_back(new Triangle("triangle", 'u', compositeur->randomizedPoints[55], Element::TRACE_POINTILLES));
        collectionDesElements.push_back(new Vecteur("vecteur", 'i', compositeur->randomizedPoints[111], &Florence));
        collectionDesElements.push_back(new Coordonnees("coordonnees", 'o', Element::TRACE_REMPLIR));
        collectionDesElements.push_back(new Distance("distance", 'p'));
        collectionDesElements.push_back(new Angle("angle", 'q', compositeur->randomizedPoints[55], &Florence, &Annabelle));
        collectionDesElements.push_back(new InterSectionDoubleCercle("intersectionDC", 's'));
        collectionDesElements.push_back(new CercleInscrit("inscrit", 'd'));
        collectionDesElements.push_back(new CercleDouble("cercles", 'f'));
        collectionDesElements.push_back(new EllipseDouble("ellipse", 'g'));
        collectionDesElements.push_back(new CerclesImmenses("cerclesImmenses", 'h'));
        collectionDesElements.push_back(new MultiplesTangentes("multiplesTangentes", 'j'));
        collectionDesElements.push_back(new MultiplesTangentesParalleles("multiplesTangentesParalleles", 'k'));
        collectionDesElements.push_back(new XenakisCercle("xenakisCercle", 'w', &Florence));
        collectionDesElements.push_back(new XenakisDroite("xenakisDroite", 'x', &Florence));
        collectionDesElements.push_back(new XenakisBaton("xenakisBaton", 'c', &Florence));
        collectionDesElements.push_back(new XenakisData("xenakisData", 'v', &Florence));

        for(dictIterator = collectionDesElements.begin();
                dictIterator != collectionDesElements.end();
                ++dictIterator)
        {
            addOscChild(*dictIterator);
        }
    };
};

/** \brief
 */
class scenePassageATraversCarresBlanc :
    public ofxScene,
    public ofxOscRouterNode
{
public:
    Tools * compositeur;
    bool bDrawMe;

    scenePassageATraversCarresBlanc() :
        ofxScene(),
        ofxOscRouterNode("carresBlancs"),
        compositeur(Tools::instance()),
        bDrawMe(true)
    {
        addOscMethod("draw");
    };

    void processOscCommand(const string& command, const ofxOscMessage& m)
    {
        ofLogVerbose() << "Draw scenePassageATraversCarresBlanc : " << command;

        if(isMatch(command, "draw"))
        {
            if(validateOscSignature("([TFif])", m))
            {
                if(getArgAsBoolUnchecked(m, 0) == true)
                {
                    bDrawMe = true;
                }
                else
                {
                    bDrawMe = false;
                }
            }
        }
    };

    /** \brief Initialisation du monde physique
     */
    void setup()
    {
        nbBoxes = 200;
        rayonRepulsion = 100;
        box2d.init();
        box2d.setGravity(0, 0);
        box2d.createBounds();
        box2d.setFPS(30.0);
        init();
    };

    /** \brief Initialisation des objets du monde physique
     */
    void init()
    {
        collectionBoxes.clear();

        for(unsigned int i(0); i < nbBoxes; i++)
        {
            float s(ofRandom(20, 50));
            collectionBoxes.push_back(ofPtr<ofxBox2dRect>(new ofxBox2dRect));
            collectionBoxes.back().get()->setPhysics(50, 1., 10.);
            collectionBoxes.back().get()->setup(box2d.getWorld(),
                                                ofRandom(ofGetWidth()),
                                                ofRandom(ofGetHeight()),
                                                s, s);
        }
    };

    /** \brief Callback d'update openFrameworks : permet l'animation des boxes
     *
     * \param dt float Le segment temporel attendu par ofxScreenCurtain
     * \return void
     * \see ofxScreenCurtain::update(float)
     *
     */
    void update(float dt)
    {
        box2d.update();

        for(unsigned int i(0); i < nbBoxes; i++)
        {
            if(Florence.distance(collectionBoxes[i].get()->getPosition()) < Florence.offset())
            {
                collectionBoxes[i].get()->addRepulsionForce(Florence, 5);
            }
            else if(Annabelle.distance(collectionBoxes[i].get()->getPosition()) < Annabelle.offset())
            {
                collectionBoxes[i].get()->addRepulsionForce(Annabelle, 5);
            }
            else
            {
                //collectionBoxes[i].get()->addAttractionPoint(obstacle, .1);
            }
        }
    };

    /** \brief Effectue le rendu des objets dans la scène
     */
    void draw()
    {
        if(bDrawMe)
        {
            for(unsigned int i(0); i < collectionBoxes.size(); i++)
            {
                ofPushStyle();
                ofFill();
                ofSetHexColor(0xFFFFFF);
                collectionBoxes[i].get()->draw();
                ofPopStyle();
            }
        }
    };

    /** \brief Effectue les modifications nécessaires suite au redimensionnement de l'affichage
     *
     * \param w int Width
     * \param h int Height
     * \return void
     */
    void windowResized(int w, int h)
    {
        ofLogVerbose("Scenes::windowResized") << w << "x" << h;
        box2d.createBounds(0, 0, w, h);
    };

    void pauseScene()
    {
        for(unsigned int i(0); i < collectionBoxes.size(); i++)
        {
            collectionBoxes[i].get()->body->SetAwake(false);
        }
    };

    void resumeScene()
    {
        for(unsigned int i(0); i < collectionBoxes.size(); i++)
        {
            collectionBoxes[i].get()->body->SetAwake(true);
        }
    };

    unsigned int nbBoxes; /**< détermine le nombre d'objets dans la scène */
    ofxBox2d box2d; /**< le monde Box2d */
    vector <ofPtr<ofxBox2dRect> > collectionBoxes; /**< Collection d'objets boite */
    unsigned int rayonRepulsion;
};


