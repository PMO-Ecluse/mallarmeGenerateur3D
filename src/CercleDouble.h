#pragma once

#include "Element.h"

class CercleDouble :
    public Element
{
public:
    CercleDouble(string nom, char key);
    CercleDouble(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~CercleDouble();
    void draw();
};
