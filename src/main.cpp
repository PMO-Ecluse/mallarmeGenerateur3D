#include "ofMain.h"
#include "Core.h"
#include "Danseur.h"

Danseur Annabelle("annabelle");
Danseur Florence("florence");

//========================================================================
int main()
{
#ifdef DEBUG
    ofSetupOpenGL(1024, 768, OF_WINDOW);
    ofSetLogLevel("mallarme3D", OF_LOG_VERBOSE);
    ofSetLogLevel(OF_LOG_VERBOSE);
    ofSetEscapeQuitsApp(true);
#else
    ofSetupOpenGL(1366, 768, OF_FULLSCREEN);
    ofSetLogLevel(OF_LOG_SILENT);
    ofSetLogLevel("mallarme3D", OF_LOG_NOTICE);
    ofSetEscapeQuitsApp(true);
    // Disable escape key to quit application : uncomment for production
#endif // DEBUG
    ofRunApp(new Core());
    return 0;
}
