#pragma once

#include "Element.h"

class Angle :
    public Element
{
public:
    Angle(string nom, char key, ofVec2f * _pointAngle, TypeTrace _type = TRACE_REMPLIR);
    Angle(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointAngle, ofVec2f * _pointDestination, TypeTrace _type = TRACE_REMPLIR);
    virtual ~Angle();
    void draw();

    void pointAngle(ofVec2f * _point)
    {
        _pointAngle = _point;
    };
    ofVec2f * pointAngle()
    {
        return _pointAngle;
    };

private:
    ofVec2f * _pointAngle;
};
