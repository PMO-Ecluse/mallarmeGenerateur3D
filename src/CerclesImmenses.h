#pragma once

#include "Element.h"

class CerclesImmenses :
    public Element
{
public:
    CerclesImmenses(string nom, char key);
    CerclesImmenses(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~CerclesImmenses();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();

    void nombreDeCercles(int _nombre)
    {
        _nombreDeCercles = (int) ofWrap(_nombre, 8, 30);
    };
    int nombreDeCercles()
    {
        return _nombreDeCercles;
    };

private:
    int _nombreDeCercles;
};
