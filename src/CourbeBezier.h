#pragma once

#include "Element.h"

class CourbeBezier :
    public Element
{
public:
    CourbeBezier(string nom, char key, ofVec2f * _pointControlA, ofVec2f * _pointControlB);
    CourbeBezier(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointControlA, ofVec2f * _pointControlB, ofVec2f * _pointDestination);
    virtual ~CourbeBezier();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();

    void showVecteur(bool _val)
    {
        _showVecteur = _val;
    };
    bool showVecteur()
    {
        return _showVecteur;
    };

private:
    ofVec2f * pointControlA;
    ofVec2f * pointControlB;
    bool _showVecteur;
};
