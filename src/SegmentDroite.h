#pragma once

#include "Element.h"

class SegmentDroite :
    public Element
{
public:
    SegmentDroite(string nom, char key, TypeTrace _type = TRACE_LIGNE);
    SegmentDroite(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination, TypeTrace _type = TRACE_LIGNE);
    virtual ~SegmentDroite();
    void draw();
};
