#pragma once

#include "ofMain.h"
#include "ofxOscRouterNode.h"
#include "ofxAnimatableFloat.h"

#include "Tools.h"

class Danseur :
    public ofVec2f,
    public ofxOscRouterNode
{
public:
    enum Representation {DANSEUR_TIME = 1,
                         DANSEUR_XPOS = 2,
                         DANSEUR_YPOS = 4,
                         DANSEUR_ANGLE = 8,
                         DANSEUR_CONSTANTE = 16,
                         DANSEUR_ALL = 255,
                        };
    Danseur(const string& nomDanseur);
    virtual ~Danseur();

    void setup();
    void setup(ofEventArgs & args)
    {
        setup();
    };
    void update();
    void update(ofEventArgs & args)
    {
        update();
    };
    void draw();
    void draw(ofEventArgs & args)
    {
        draw();
    };
    void apparait(float valDestination = 1);
    void disparait();
    void processOscCommand(const string& command, const ofxOscMessage& m);

    void Alpha(float _valAlpha)
    {
        _alpha.reset(ofClamp(_valAlpha, 0, 1));
    };
    float Alpha()
    {
        return _alpha.val();
    };

    void offset(float width)
    {
        _offset = width;
    };
    float offset()
    {
        return _offset;
    };

    bool Dessinable()
    {
        return bDessinable;
    };
    void Dessinable(bool _val)
    {
        bDessinable = _val;
    };

    void Toggle()
    {
        bDessinable = !bDessinable;
    };

    void Animable(bool _val)
    {
        bAnimable = _val;
    };
    bool Animable()
    {
        return bAnimable;
    };

    friend ostream& operator<<(ostream& os, const Danseur& _d);

    unsigned char toBeDrawn;

protected:
    std::string nomDanseur;
    Tools * compositeur;

private:
    virtual void initialiseOscMethodes();

    bool bDessinable;
    bool bAnimable;
    uint32_t _offset;
    ofTrueTypeFont * _ttfText;
    ofTrueTypeFont * _ttfTextHuge;
    ofxAnimatableFloat _alpha;
    ofxAnimatableFloat _coteAngle;
    std::string _cote;
    ofVec2f _cotePosition;
    uint16_t _coteAngleOffset;
    bool bHugeText;
    bool bStopCounter;
};

extern Danseur Annabelle;
extern Danseur Florence;
