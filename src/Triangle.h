#pragma once

#include "Element.h"

class Triangle :
    public Element
{
public:
    Triangle(string nom, char key, ofVec2f * _pointIntermediaire, TypeTrace _type = TRACE_LIGNE);
    Triangle(string nom, char key,
             ofVec2f * _pointOrigine,
             ofVec2f * _pointIntermediaire,
             ofVec2f * _pointDestination, TypeTrace _type = TRACE_LIGNE);
    virtual ~Triangle();
    void draw();
private:
	ofVec2f * pointIntermediaire;
};
