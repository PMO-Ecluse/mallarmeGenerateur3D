#include "Tools.h"

Tools * Tools::singleton = NULL;
ofStyle * Tools::styleStandard;

/// \brief Constructeur du singleton de Tools.
///
/// Ceci est un nœud OSC singleton dont le nom est "configureCompositions".
///
Tools::Tools() :
    ofxOscRouterNode("configure")
{
    styleStandard = new ofStyle();
    styleStandard->bFill                 = false;
    styleStandard->lineWidth             = 3.0f;
    styleStandard->curveResolution       = 10;
    styleStandard->circleResolution      = 80;
    styleStandard->smoothing             = true;
    styleStandard->color.set(1, 1, 1, 1);
    styleStandard->bgColor.set(0);
    styleStandard->drawBitmapMode        = OF_BITMAPMODE_SIMPLE;
    addOscMethod("lineWidth");
    addOscMethod("color");
    addOscMethod("curveResolution");
    addOscMethod("circleResolution");
}

/// \brief Accesseur au singleton
///
/// \return Tools* Pointeur vers le singleton
///
///
Tools * Tools::instance()
{
    if(!singleton)     // Only allow one instance of class to be generated.
    {
        singleton = new Tools();
    }

    return singleton;
}

/// \brief
///
/// \param command const string&
/// \param m const ofxOscMessage&
/// \return void
///
///
void Tools::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "lineWidth"))
    {
        if(validateOscSignature("([if])", m))
        {
            styleStandard->lineWidth = getArgAsFloatUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "color"))
    {
        if(validateOscSignature("([f][f][f][f])", m))
        {
            styleStandard->color.set(getArgAsFloatUnchecked(m, 0),
                                     getArgAsFloatUnchecked(m, 1),
                                     getArgAsFloatUnchecked(m, 2),
                                     getArgAsFloatUnchecked(m, 3)
                                    );
        }
    }
    else if(isMatch(command, "curveResolution"))
    {
        if(validateOscSignature("([if])", m))
        {
            styleStandard->curveResolution = getArgAsIntUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "circleResolution"))
    {
        if(validateOscSignature("([if])", m))
        {
            styleStandard->circleResolution = getArgAsIntUnchecked(m, 0);
        }
    }
}
/// \brief Initialisation de points aléatoires disposés sur le plan de projection
///
/// \return void
///
///
void Tools::initializeRandomizedPoints()
{
    for(uint8_t i = 0; i < 255; i++)
    {
        randomizedPoints.push_back(new ofVec2f(ofRandom(-1, 2) * ofGetWidth(), ofRandom(-1, 2) * ofGetHeight()));
    }
}

/// \brief Calcule la distance entre deux points
///
/// \param _origine const ofVec2f& Le premier point
/// \param _destination const ofVec2f& Le second point
/// \return float La distance entre les deux points
///
///
float Tools::getDistance(const ofVec2f & _origine, const ofVec2f & _destination)
{
    return _origine.distance(_destination);
}

/// \brief Calcule l'angle (positif) entre deux points
///
/// L'angle est calculé entre la droite horizontale passant par le
/// premier point et la droite passant par les deux points.
///
/// \param _origine const ofVec2f& Le premier point
/// \param _destination const ofVec2f& Le second point
/// \return float L'angle calculé
///
///
float Tools::getAngle(const ofVec2f & _origine, const ofVec2f & _destination)
{
    ofVec2f d(_destination - _origine);
    float angle(ofRadToDeg(acos(d.x / getDistance(_origine, _destination))));

    if(d.y < 0)
    {
        angle *= -1;
    }

    return angle;
}

/// \brief Calcule le point d'intersection entre deux segments.
///
/// Calcule le point d'intersection P entre les droites issues
/// des segments [AB] et [CD].
///
/// \param A const ofVec2f& Point du segment [AB]
/// \param B const ofVec2f& Point du segment [AB]
/// \param C const ofVec2f& Point du segment [CD]
/// \param D const ofVec2f& Point du segment [CD]
/// \return ofVec2f
/// \see http://openclassrooms.com/forum/sujet/calcul-du-point-d-intersection-de-deux-segments-21661#message-88468979
///
ofVec2f Tools::getIntersectionPoint(const ofVec2f& A, const ofVec2f& B, const ofVec2f& C, const ofVec2f& D)
{
    ofVec2f I(B.x - A.x, B.y - A.y);
    ofVec2f J(D.x - C.x, D.y - C.y);
    float k(0);
    float diviseur(I.x * J.y - I.y * J.x);

    if(diviseur != 0)
    {
        k = (J.x * A.y
             - J.x * C.y
             - J.y * A.x
             + J.y * C.x
            ) / diviseur;
    }

    return ofVec2f(A + k * I);
}
