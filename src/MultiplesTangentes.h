#pragma once

#include "Element.h"
#include "Tangente.h"

class MultiplesTangentes :
    public Element
{
public:
    MultiplesTangentes(string nom, char key);
    MultiplesTangentes(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~MultiplesTangentes();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();

    Tangente * tangente()
    {
        return _tangente;
    };

    void nombreDeTangentes(int _nombre)
    {
        _nombreDeTangentes = ofClamp(_nombre, 1, _nombreDeTangentesMax);
        _step = _nombreDeTangentesMax / _nombreDeTangentes;
    };
    int nombreDeTangentes()
    {
        return _nombreDeTangentes;
    };
    void nombreDeTangentesMax(int _nombre)
    {
        if(_nombre > 1)
        {
            _nombreDeTangentesMax = _nombre;
            nombreDeTangentes(nombreDeTangentes());
        }
    };
    int nombreDeTangentesMax()
    {
        return _nombreDeTangentesMax;
    };

protected:
    int _step;

private:
    Tangente * _tangente;
    int _nombreDeTangentes;
    int _nombreDeTangentesMax;
};
