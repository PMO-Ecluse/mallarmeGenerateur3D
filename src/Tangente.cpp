#include "Tangente.h"

Tangente::Tangente(string nom, char key, float rayon, float angle) :
    Element(nom, key),
    _angle(angle),
    _rayon(rayon)
{
    initialiseOscMethodes();
}

Tangente::Tangente(string nom, char key, ofVec2f * _pointOrigine, float rayon, float angle) :
    Tangente(nom, key, angle, rayon)
{
    Origine(_pointOrigine);
}

Tangente::~Tangente()
{
}

void Tangente::initialiseOscMethodes()
{
    addOscMethod("angle");
    addOscMethod("rayon");
}

void Tangente::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "angle"))
    {
        if(validateOscSignature("([if])", m))
        {
            angle(getArgAsFloatUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "rayon"))
    {
        if(validateOscSignature("([if])", m))
        {
            rayon(getArgAsFloatUnchecked(m, 0));
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void Tangente::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        ofVec2f PointTangent(Origine()->x + rayon() * cosf(ofDegToRad(angle())),
                             Origine()->y + rayon() * sinf(ofDegToRad(angle())));
        ofPushMatrix();
        ofTranslate(PointTangent);
        ofRotateZ(angle());
        ofLine(0, ofGetHeight() * 2, 0, -ofGetHeight() * 2);
        ofPopMatrix();
        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

