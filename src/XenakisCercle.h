#pragma once

#include "Element.h"
#include "Xenakis.h"

class XenakisCercle :
    public Xenakis
{
public:
    XenakisCercle(string nom, char key, ofVec2f * _pointOrigine);
    virtual ~XenakisCercle();
    void draw();
};
