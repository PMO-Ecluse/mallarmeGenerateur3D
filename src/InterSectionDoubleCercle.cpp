#include "InterSectionDoubleCercle.h"

InterSectionDoubleCercle::InterSectionDoubleCercle(string nom, char key) :
    Element(nom, key),
    _vecteur(new Vecteur(ofxOscRouterNode::getFirstOscNodeAlias() + "-vecteur", '@'))
{
    initialiseOscMethodes();
}

InterSectionDoubleCercle::InterSectionDoubleCercle(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    InterSectionDoubleCercle(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

InterSectionDoubleCercle::~InterSectionDoubleCercle()
{
}

void InterSectionDoubleCercle::initialiseOscMethodes()
{
    addOscMethod("offset");
}

void InterSectionDoubleCercle::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "offset"))
    {
        if(validateOscSignature("([if])", m))
        {
            offset(getArgAsIntUnchecked(m, 0));
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void InterSectionDoubleCercle::draw()
{
    if(Dessinable())
    {
        float rayon(200);
        float distance(Tools::getDistance(*Origine(), *Destination()));

        if(distance < rayon * 2)
        {
            ofPushStyle();
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, Alpha() * 255);
            ofSetLineWidth(lineWidth());
            float angle(Tools::getAngle(*Origine(), *Destination()));
            ofPushMatrix();
            ofTranslate(Origine()->middled(*Destination()));
            ofRotateZ(angle);
            float hauteur(std::sqrt(rayon * rayon - distance * distance / 4));
            ofVec2f intersectionA(0, hauteur);
            ofVec2f intersectionB(0, -hauteur);
            _vecteur->Dessinable(Dessinable());
            _vecteur->Alpha(Alpha());
            _vecteur->lineWidth(lineWidth());
            _vecteur->Origine(&intersectionA);
            _vecteur->Destination(&intersectionB);
            _vecteur->draw();
            ofPopMatrix();
            ofDisableAlphaBlending();
            ofPopStyle();
        }
    }
}

