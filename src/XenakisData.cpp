#include "XenakisData.h"

XenakisData::XenakisData(string nom, char key, ofVec2f * _pointOrigine, int _font) :
    Xenakis(nom, key, _pointOrigine),
    _ttfText(new ofTrueTypeFont())
{
    fontSize(_font); ///< Charge aussi la police
}

XenakisData::~XenakisData()
{
}

void XenakisData::initialiseOscMethodes()
{
    addOscMethod("fontSize");
}

void XenakisData::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "fontSize"))
    {
        if(validateOscSignature("([if])", m))
        {
            fontSize(getArgAsIntUnchecked(m, 0));
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void XenakisData::draw()
{
    if(Dessinable())
    {
        if(!xenakisQueue.empty())
        {
            ofPushStyle();
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, Alpha() * 255);
            ofSetLineWidth(lineWidth());
            ofVec2f previousPoint(xenakisQueue.front());

            for(itXenakisQueue = xenakisQueue.begin(); itXenakisQueue != xenakisQueue.end(); itXenakisQueue++)
            {
                ofPushMatrix();
                ofTranslate(*itXenakisQueue);
                ofRotateZ(Tools::getAngle(previousPoint, *itXenakisQueue));
                _ttfText->drawString(ofToString((*itXenakisQueue).x), 0, -offset());
                _ttfText->drawString(ofToString((*itXenakisQueue).y), 0, offset());
                ofPopMatrix();
                previousPoint.set(*itXenakisQueue);
            }

            ofDisableAlphaBlending();
            ofPopStyle();
        }
    }
}

