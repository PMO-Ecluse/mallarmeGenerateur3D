#include "CerclesImmenses.h"

CerclesImmenses::CerclesImmenses(string nom, char key) :
    Element(nom, key),
    _nombreDeCercles(4 * M_PI)
{
    initialiseOscMethodes();
}

CerclesImmenses::CerclesImmenses(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    CerclesImmenses(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

CerclesImmenses::~CerclesImmenses()
{
}

void CerclesImmenses::initialiseOscMethodes()
{
    addOscMethod("nombre");
}

void CerclesImmenses::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "nombre"))
    {
        if(validateOscSignature("([if])", m))
        {
            nombreDeCercles(getArgAsIntUnchecked(m, 0));
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void CerclesImmenses::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        ofVec2f milieu(Origine()->getMiddle(*Destination()));

        for(int i = 0; i < 200; i += 13)
        {
            if(i % 22 < nombreDeCercles())
            {
                ofCircle(*compositeur->randomizedPoints[i],
                         Tools::getDistance(
                             Florence,
                             *compositeur->randomizedPoints[i + 1]
                         )
                        );
                ofCircle(*compositeur->randomizedPoints[i + 2],
                         Tools::getDistance(
                             Annabelle,
                             *compositeur->randomizedPoints[i + 3]
                         )
                        );
            }
        }

        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

